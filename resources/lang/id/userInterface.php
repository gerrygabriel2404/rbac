<?php

return [

    /*
    |--------------------------------------------------------------------------
   
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'name'=> 'Nama',
    'language'=>'Bahasa',
    'hasLogin' => 'Anda Berhasil Login!',
    'logout' => 'Keluar',
    'login' => 'Masuk',
    'welcome' => 'Selamat Datang',
    'register' => 'Registrasi',
    'email'=> 'Alamat Surel',
    'password'=> 'Kata Sandi',
    'confirmPassword'=> 'Konfirmasi Sandi',
    'forgotPassword'=> 'Lupa katasandi anda?'
];
