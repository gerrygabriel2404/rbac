<?php

return [

    /*
    |--------------------------------------------------------------------------
   
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'name'=> 'Name',
    'language'=>'English',
    'hasLogin' => 'You are logged in!',
    'logout' => 'Logout',
    'login' => 'Login',
    'welcome' => 'Welcome',
    'register' => 'Register',
    'email'=> 'E-Mail Address',
    'password'=> 'Password',
    'confirmPassword'=> 'Confirm Password',
    'forgotPassword'=> 'Forgot Your Password?'
];
